<?php

namespace App\Http\Controllers\Api;

// use App\Models\BloodType;
// use App\Models\Category;
// use App\Models\City;
// use App\Models\Contact;
// use App\Models\DonationRequest;
// use App\Models\Governorate;
// use App\Models\Notification;
// use App\Models\Post;
// use App\Models\Client;
// use App\Models\RequestLog;
// use App\Models\Log;
// use App\Models\Token;
use App\Model\RequestLog;
use App\HospitalsAndDoctor;
use App\Medical;
use App\Pharmacy;


use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class MainController extends Controller
{
    public function Hospitals(Request $request)
    {
        // dd('tset');
        $HospitalsAndDoctor = HospitalsAndDoctor::where('type' , 'hospital')->where(function($HospitalsAndDoctor) use($request){
            if ($request->input('name'))
            {
                $HospitalsAndDoctor->where('name',$request->name);
            }
        })->latest()->get();
        return $this->responseJson(1, 'success', $HospitalsAndDoctor);
    }

    public function Doctors(Request $request)
    {
        // dd('tset');
        $HospitalsAndDoctor = HospitalsAndDoctor::where('type' , 'doctor')->where(function($HospitalsAndDoctor) use($request){
            if ($request->input('name'))
            {
                $HospitalsAndDoctor->where('name',$request->name);
            }
        })->latest()->get();
        return $this->responseJson(1, 'success', $HospitalsAndDoctor);
    }
    
    public function Medical(Request $request)
    {
        // dd('tset');
        $Medical = Medical::where(function($Medical) use($request){
            if ($request->input('name'))
            {
                $Medical->where('name',$request->name);
            }
        })->latest()->get();
        return $this->responseJson(1, 'success', $Medical);
    }

    public function Pharmacy(Request $request)
    {
        // dd('tset');
        $Pharmacy = Pharmacy::where(function($Pharmacy) use($request){
            if ($request->input('name'))
            {
                $Pharmacy->where('name',$request->name);
            }
        })->latest()->get();
        return $this->responseJson(1, 'success', $Pharmacy);
    }
    public function Products(Request $request)
    {
        $Product = Product::with('Images')->where(function($Product) use($request){
            if ($request->input('sub_category_id'))
            {
                $Product->where('sub_category_id',$request->sub_category_id);
            }else if($request->input('search'))
            {
                $Product->where('title',  'like', '%' .  $request->search . '%' );
            }
        })->latest()->get();
        return responseJson(1, 'success', $Product);
    }

    public function ProductsSpecialPrice(Request $request)
    {
        $Product = Product::with('Images')->where('Hoot_deals','>', 0 )->where(function($Product) use($request){
            if ($request->input('sub_category_id'))
            {
                $Product->where('sub_category_id',$request->sub_category_id);
            }
        })->latest()->get();
        return responseJson(1, 'success', $Product);
    }

    public function SliderImages(Request $request)
    {
        $Image = ImageSlider::get();
        return responseJson(1, 'success', $Image);
    }

    public function mail(Request $request)
    {
        RequestLog::create(['content' => $request->all(),'service' => 'mail']);
        $validator = validator()->make($request->all(),[
            'titel' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails())
        {
            return responseJson(0,$validator->errors()->first(),$validator->errors());
        }
        $User = User::where('api_token' , $request->api_token)->first();
        $Mail = New Mail ;
        $Mail->titel = $request->titel;
        $Mail->content = $request->content;
        $Mail->user_id = $User->id;
        $Mail->save();
        return responseJson(1,'تم الاضافة بنجاح');
    }


    function responseJson($status, $msg, $data = null)
    {
        $response = [
            'status' => $status,
            'msg' => $msg,
            'data' => $data
        ];
        return response()->json($response);
    }


}
