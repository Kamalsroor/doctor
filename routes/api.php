<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register','AuthController@register');

Route::middleware('auth:api')->group(function () {
    Route::get('hospital','MainController@Hospitals');
    Route::get('doctor','MainController@Doctors');
    Route::get('medical','MainController@Medical');
    Route::get('pharmacy','MainController@Pharmacy');
    // return $request->user();
});





// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
